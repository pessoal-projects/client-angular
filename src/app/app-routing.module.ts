import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';
import { ClientAddComponent } from './client-add/client-add.component';

const routes: Routes = [
	{
	    path: 'clients',
	    component: ClientsComponent,
	    data: { title: 'List of Clients' }
	 },
	 {
	    path: 'client-add',
	    component: ClientAddComponent,
	    data: { title: 'Add Client' }
	  },

	   { path: '',
	    redirectTo: '/clients',
	    pathMatch: 'full'
	  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
