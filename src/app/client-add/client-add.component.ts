import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';


@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.css']
})
export class ClientAddComponent implements OnInit {

displayedColumns: string[] = ['id', 'name'];
data: Client[] = [];
isLoadingResults = true;


productForm: FormGroup;
name:string='';
creditLimit:number=null;
typeClient:string='';


 constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {

	this.productForm = this.formBuilder.group({
	    'name' : [null, Validators.required],
	    'creditLimit' : [null, Validators.required],
	    'typeClient' : [null, Validators.required]
	    
	  });



  }

  	 onFormSubmit(form:NgForm) {
	  this.isLoadingResults = true;
	  this.api.addClient(form)
	    .subscribe(res => {
	        let id = res['_id'];
	        this.isLoadingResults = false;
	        this.router.navigate(['/clients']);
	      }, (err) => {
	        console.log(err);
	        this.isLoadingResults = false;
	      });
	}

 

}
