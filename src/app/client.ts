export class Client {
  id: number;
  name: string;
  interestCharge: number;
  creditLimit: number;
  typeClient: string;
}