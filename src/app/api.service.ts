import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Client } from './client';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = "http://localhost:8080/";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

	getClients (): Observable<Client[]> {
	  return this.http.get<Client[]>(apiUrl)
	    .pipe(
	      tap(heroes => console.log('fetched clients')),
	      catchError(this.handleError('getClient', []))
	    );
	}


	addClient (client): Observable<Client> {
		  return this.http.post<Client>(apiUrl+'/add', client, httpOptions).pipe(
		    tap((client: Client) => console.log(`added client w/ id=${client.id}`)),
		    catchError(this.handleError<Client>('addClient'))
		  );
		}



		private handleError<T> (operation = 'operation', result?: T) {
		  return (error: any): Observable<T> => {

		    // TODO: send the error to remote logging infrastructure
		    console.error(error); // log to console instead

		    // Let the app keep running by returning an empty result.
		    return of(result as T);
		  };
		}

}
